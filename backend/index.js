const bcrypt = require('bcrypt')
const SALT_ROUNDS =10 
const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
dotenv.config()
const TOKEN_PASSWORD = process.env.TOKEN_PASSWORD
const mysql = require("mysql")
const connection = mysql.createConnection({
    host : 'localhost' ,
    user : 'ktl.0707' ,
    password : '#KTL070744' ,
    database : 'ระบบสมัครโรงเรียนดนตรี'
})
connection.connect()
const express = require('express')
const app = express()
const port = 4000

function authenticateToken(req,res,next) {

    const authHeader = req.headers['authorization']
    console.log(authHeader)
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) return res.sendStatus(401)
    jwt.verify(token,TOKEN_PASSWORD,(err,user) => {
        if(err) {
            console.log(err)
            return res.sendStatus(403) 
        }
        else{
            req.user = user
            next()
        } 
    }) 
}

app.post("/register_user", authenticateToken ,(req,res) => {

    let name = req.query.name
    let surname = req.query.surname
    let username = req.query.username
    let password = req.query.password

    bcrypt.hash(password,SALT_ROUNDS,(err,hash) => {
        let query = `INSERT INTO learnerdata
                    (Name, Surname, Username, Password, IsAdmin)
                    VALUES('${name}','${surname}','${username}','${hash}', false)`
        connection.query(query,(err,rows) =>{
            if(err) {
                console.log(err)
                res.json({
                        "status" : "400",
                        "message" : "Error insertting into data"
                })
            }
            else{
                res.json({
                    "status" : "200",
                    "message" : "Adding new worker successful"
                })
            }
        })
    })
})

app.post("/update_user",(req,res) => {

    let id = req.query.id
    let name = req.query.name
    let surname = req.query.surname
    let query = `UPDATE learnerdata SET Name = '${name}' ,
                Surname = '${surname}',
                WHERE ID = '${id}'`
    console.log(query)            
    connection.query(query,(err,rows) =>{
        if(err) {res.json(err)}
        else{res.json(rows)}
    })
})

app.post("/list_user",(req,res) => {
    let query = "SELECT * from learnerdata"
    connection.query(query,(err,rows) =>{
        if(err) {res.json(err)}
        else{res.json(rows)}
    })
})

app.post("/delete_user",  (req, res) =>{

    let id = req.query.id

    let query = `DELETE FROM learnerdata WHERE ID=${id}`
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            console.log(err)
             res.json ({
                          "status" : "400",
                          "message" : "Error deleting record"
                      })
        }else {
             res.json({
                "status" : "200",
                "message" : "deleting user succesful"
             })
        }
})
})

app.post("/login",(req,res) => {
    let username = req.query.username
    let password = req.query.password
    let query = `SELECT * from learnerdata WHERE Username = '${username}'`
    connection.query(query,(err,rows) =>{
        if(err){console.log(err)
            res.json({
                        "status" : "400" ,
                        "message" : "Invalid Username/Password"
            })
        }else{    
            let db_password = rows[0].Password
            console.log(db_password)
            bcrypt.compare(password,db_password,(err,result) => {
                if(result){
                    let payload = {
                        "username" : rows[0].Username,
                        "dept" : rows[0].Dept,
                        "id"  : rows[0].Id
                    }
                    let token = jwt.sign(payload,TOKEN_PASSWORD,{expiresIn : '1d'})
                    res.json ({
                        "status" : "200",
                        "token" : token
                    })
           }else {
               res.json({
                   "status" : "400",
               }) }
         })
    }
  })
})

app.listen(port,() => {
console.log(`example app listening on port ${port}` )
})
